extends KinematicBody

const STRING_RIFLE = "RIFLE"
const STRING_KNIFE = "KNIFE"
const STRING_PISTOL = "PISTOL"
const STRING_UNARMED = "UNARMED"
const STRING_GRANADE = "Grenade"
const STRING_GRANADE_STICKY = "Sticky Grenade"

const MAX_HEALTH = 150
const GRAVITY = -96
const MAX_SPEED = 20
const MAX_SPRINT_SPEED = 30
const JUMP_SPEED = 100
const ACCEL = 4.5
const SPRINT_ACCEL = 18
const DEACCEL = 16
const MAX_SLOPE_ANGLE = 40
const WEAPON_NUMBER_TO_NAME = {0:STRING_UNARMED, 1:STRING_KNIFE, 2:STRING_PISTOL, 3:STRING_RIFLE}
const WEAPON_NAME_TO_NUMBER = {STRING_UNARMED:0, STRING_KNIFE:1, STRING_PISTOL:2, STRING_RIFLE:3}
const JOYPAD_DEADZONE = 0.15
const MOUSE_SENSITIVITY_SCROLL_WHEEL = 0.08
const GRENADE_THROW_FORCE = 100
const OBJECT_THROW_FORCE = 500
const OBJECT_GRAB_DISTANCE = 7
const OBJECT_GRAB_RAY_DISTANCE = 10
var MOUSE_SENSITIVITY = 0.05
var JOYPAD_SENSITIVITY = 2

var grabbed_object = null
var grenade_amounts = {STRING_GRANADE:2, STRING_GRANADE_STICKY:2}
var current_grenade = STRING_GRANADE
var grenade_scene = preload("res://Grenade.tscn")
var sticky_grenade_scene = preload("res://Sticky_Grenade.tscn")
var mouse_scroll_value = 0
var is_sprinting = false
var vel = Vector3()
var dir = Vector3()
var input_movement_vector = Vector2()
var camera
var rotation_helper
var flashlight
var animation_manager
var current_weapon_name = STRING_UNARMED
var weapons = {STRING_UNARMED:null, STRING_KNIFE:null, STRING_PISTOL:null, STRING_RIFLE:null}
var changing_weapon = false
var changing_weapon_name = STRING_UNARMED
var health = 100
var UI_status_label
var reloading_weapon = false
var simple_audio_player = preload("res://Simple_Audio_Player.tscn")
var bgm

func _ready():
	camera = $Rotation_Helper/Camera
	rotation_helper = $Rotation_Helper
	
	animation_manager = $Rotation_Helper/Model/Animation_Player
	animation_manager.callback_function = funcref(self, "fire_bullet")
	
	Input.set_mouse_mode(Input.MOUSE_MODE_CAPTURED)
	
	weapons[STRING_KNIFE] = $Rotation_Helper/Gun_Fire_Points/Knife_Point
	weapons[STRING_PISTOL] = $Rotation_Helper/Gun_Fire_Points/Pistol_Point
	weapons[STRING_RIFLE] = $Rotation_Helper/Gun_Fire_Points/Rifle_Point
	
	var gun_aim_point_pos = $Rotation_Helper/Gun_Aim_Point.global_transform.origin
	
	for weapon in weapons:
		var weapon_node = weapons[weapon]
		if weapon_node != null:
			weapon_node.player_node = self
			weapon_node.look_at(gun_aim_point_pos, Vector3(0, 1, 0))
			weapon_node.rotate_object_local(Vector3(0, 1, 0), deg2rad(180))
	
	current_weapon_name = STRING_UNARMED
	changing_weapon_name = STRING_UNARMED
	
	UI_status_label = $HUD/Panel/Gun_label
	flashlight = $Rotation_Helper/Flashlight
	
func _process(delta):
	pass
	
func _physics_process(delta):
	process_input(delta)
	process_movement(delta)
	process_UI(delta)
	if grabbed_object == null:
		process_reloading(delta)
		process_changing_weapons()

func add_ammo(additional_ammo):
	if (current_weapon_name != "UNARMED"):
		if (weapons[current_weapon_name].CAN_REFILL == true):
			weapons[current_weapon_name].spare_ammo += weapons[current_weapon_name].AMMO_IN_MAG * additional_ammo

func add_health(additional_health):
	health += additional_health
	health = clamp(health, 0, MAX_HEALTH)

func process_reloading(delta):
	if reloading_weapon == true:
		var current_weapon = weapons[current_weapon_name]
		if current_weapon != null:
			current_weapon.reload_weapon()
		reloading_weapon = false

func process_UI(delta):
	if current_weapon_name == STRING_UNARMED or current_weapon_name == STRING_KNIFE:
		UI_status_label.text = "HEALTH: " + str(health) + \
				"\n" + current_grenade + ": " + str(grenade_amounts[current_grenade])
	else:
		var current_weapon = weapons[current_weapon_name]
		UI_status_label.text = "HEALTH: " + str(health) + \
			"\nAMMO: " + str(current_weapon.ammo_in_weapon) + "/" + str(current_weapon.spare_ammo) + \
				"\n" + current_grenade + ": " + str(grenade_amounts[current_grenade])

func add_grenade(additional_grenade):
	grenade_amounts[current_grenade] += additional_grenade
	grenade_amounts[current_grenade] = clamp(grenade_amounts[current_grenade], 0, 10)

func process_changing_weapons():
	if changing_weapon == true:
		var weapon_unequipped = false
		var current_weapon = weapons[current_weapon_name]
		if current_weapon == null :
			weapon_unequipped = true
		else:
			if current_weapon.is_weapon_enabled == true:
				weapon_unequipped = current_weapon.unequip_weapon()
			else:
				weapon_unequipped = true
		if weapon_unequipped == true:
			var weapon_equipped = false
			var weapon_to_equip = weapons[changing_weapon_name]
			if weapon_to_equip == null:
				weapon_equipped = true
			else:
				if weapon_to_equip.is_weapon_enabled == false:
					weapon_equipped = weapon_to_equip.equip_weapon()
				else:
					weapon_equipped = true
					
			if weapon_equipped == true:
				changing_weapon = false
				current_weapon_name = changing_weapon_name
				changing_weapon_name = ""

func process_input(delta):
	movement_input()	
	change_weapons_input()
	fire_weapon_input()
	reloading_weapon_input()
	process_granade_input()
	joypad_move_input()
	joypad_view_input(delta)
	process_grabbing()
			
	if Input.is_action_just_pressed("ui_cancel"):
		if Input.get_mouse_mode() == Input.MOUSE_MODE_VISIBLE:
			Input.set_mouse_mode(Input.MOUSE_MODE_CAPTURED)
		else:
			Input.set_mouse_mode(Input.MOUSE_MODE_VISIBLE)
	if Input.is_action_just_pressed("flashlight"):
		if flashlight.is_visible_in_tree():
			flashlight.hide()
		else:
			flashlight.show()

func process_grabbing():
	if Input.is_action_just_pressed("fire") and current_weapon_name == STRING_UNARMED:
		if grabbed_object == null:
			var state = get_world().direct_space_state
			var center_position = get_viewport().size/2
			var ray_from = camera.project_ray_origin(center_position)
			var ray_to = ray_from + camera.project_ray_normal(center_position) * OBJECT_GRAB_RAY_DISTANCE
			var ray_result = state.intersect_ray(ray_from, ray_to, [self, $Rotation_Helper/Gun_Fire_Points/Knife_Point/Area])
			if ray_result != null:
				if ray_result.has("collider"):
					if ray_result["collider"] is RigidBody:
						grabbed_object = ray_result["collider"]
						grabbed_object.mode = RigidBody.MODE_STATIC
						grabbed_object.collision_layer = 0
						grabbed_object.collision_mask = 0
		else:
			grabbed_object.mode = RigidBody.MODE_RIGID
			grabbed_object.apply_impulse(Vector3(0, 0, 0), -camera.global_transform.basis.z.normalized() * OBJECT_THROW_FORCE)
			grabbed_object.collision_layer = 1
			grabbed_object.collision_mask = 1
			grabbed_object = null
	if grabbed_object != null:
		grabbed_object.global_transform.origin = camera.global_transform.origin + (-camera.global_transform.basis.z.normalized() * OBJECT_GRAB_DISTANCE)

func process_granade_input():
	if Input.is_action_just_pressed("change_grenade"):
		match current_grenade:
			STRING_GRANADE:
				current_grenade = STRING_GRANADE_STICKY
			STRING_GRANADE_STICKY:
				current_grenade = STRING_GRANADE
	if Input.is_action_just_pressed("fire_grenade"):
		if grenade_amounts[current_grenade] > 0:
			grenade_amounts[current_grenade] -= 1
			
			var grenade_clone
			match current_grenade:
				STRING_GRANADE :
					grenade_clone = grenade_scene.instance()
				STRING_GRANADE_STICKY:
					grenade_clone = sticky_grenade_scene.instance()
					grenade_clone.player_body = self
			get_tree().root.add_child(grenade_clone)
			grenade_clone.global_transform = $Rotation_Helper/Grenade_Toss_Pos.global_transform
			grenade_clone.apply_impulse(Vector3(0, 0, 0), grenade_clone.global_transform.basis.z * GRENADE_THROW_FORCE)

func process_movement(delta):
	dir.y = 0
	dir = dir.normalized()
	vel.y += delta * GRAVITY
	var hvel = vel
	hvel.y = 0
	var target = dir
	if is_sprinting:
		target *= MAX_SPRINT_SPEED
	else:
		target *= MAX_SPEED
	var accel
	
	if dir.dot(hvel) > 0:
		if is_sprinting:
			accel = SPRINT_ACCEL
		else:
			accel = ACCEL
	else:
		accel = DEACCEL
	
	hvel = hvel.linear_interpolate(target, accel * delta)
	vel.x = hvel.x
	vel.z = hvel.z
	vel = move_and_slide(vel, Vector3(0, 1, 0), 0.05, 4, deg2rad(MAX_SLOPE_ANGLE))

func _input(event):
	if event is InputEventMouseMotion and Input.get_mouse_mode() == Input.MOUSE_MODE_CAPTURED:
		rotation_helper.rotate_x(deg2rad(event.relative.y * MOUSE_SENSITIVITY))
		self.rotate_y(deg2rad(event.relative.x * MOUSE_SENSITIVITY * -1))
		var camera_rot = rotation_helper.rotation_degrees
		camera_rot.x = clamp(camera_rot.x, -70, 70)
		rotation_helper.rotation_degrees = camera_rot
		
	if event is InputEventMouseButton and Input.get_mouse_mode() == Input.MOUSE_MODE_CAPTURED:
		if event.button_index == BUTTON_WHEEL_UP:
			mouse_scroll_value += MOUSE_SENSITIVITY_SCROLL_WHEEL
		if event.button_index == BUTTON_WHEEL_DOWN:
			mouse_scroll_value -= MOUSE_SENSITIVITY_SCROLL_WHEEL
		mouse_scroll_value = clamp(mouse_scroll_value, 0, WEAPON_NUMBER_TO_NAME.size() - 1)
		
		if changing_weapon == false:
			if reloading_weapon == false:
				var round_mouse_scroll_value = int(round(mouse_scroll_value))
				if WEAPON_NUMBER_TO_NAME[round_mouse_scroll_value] != current_weapon_name:
					changing_weapon_name = WEAPON_NUMBER_TO_NAME[round_mouse_scroll_value]
					changing_weapon = true
					mouse_scroll_value = round_mouse_scroll_value
	
		
func movement_input():
	if is_on_floor():
		dir = Vector3()
		var cam_xform = camera.get_global_transform()
	
		if Input.is_action_pressed("movement_forward"):
			input_movement_vector.y += 1
		elif not Input.is_action_pressed("movement_backward"):
			input_movement_vector.y = 0
		if Input.is_action_pressed("movement_backward"):
			input_movement_vector.y -= 1
		elif not Input.is_action_pressed("movement_forward"):
			input_movement_vector.y = 0
			
		if Input.is_action_pressed("movement_left"):
			input_movement_vector.x += 1
		elif not Input.is_action_pressed("movement_right"):
			input_movement_vector.x = 0
		if Input.is_action_pressed("movement_right"):
			input_movement_vector.x -= 1
		elif not Input.is_action_pressed("movement_left"):
			input_movement_vector.x = 0
			
		if Input.is_action_pressed("movement_sprint"):
			is_sprinting = true
		else:
			is_sprinting = false
		
		input_movement_vector = input_movement_vector.normalized()
		dir += -cam_xform.basis.z * input_movement_vector.y
		dir += -cam_xform.basis.x * input_movement_vector.x
		
		if Input.is_action_pressed("movement_jump"):
			vel.y = JUMP_SPEED
		
func change_weapons_input():
	var weapon_change_number = WEAPON_NAME_TO_NUMBER[current_weapon_name]
	if Input.is_key_pressed(KEY_1):
		weapon_change_number = 0
	if Input.is_key_pressed(KEY_2):
		weapon_change_number = 1
	if Input.is_key_pressed(KEY_3):
		weapon_change_number = 2
	if Input.is_key_pressed(KEY_4):
		weapon_change_number = 3
			
	if Input.is_action_just_pressed("shift_weapon_positive"):
		weapon_change_number += 1
	if Input.is_action_just_pressed("shift_weapon_negative"):
		weapon_change_number -= 1
		
	weapon_change_number = clamp(weapon_change_number, 0, WEAPON_NUMBER_TO_NAME.size() -1)
	
	if changing_weapon == false:
		if reloading_weapon == false:
			if WEAPON_NUMBER_TO_NAME[weapon_change_number] != current_weapon_name:
				changing_weapon_name = WEAPON_NUMBER_TO_NAME[weapon_change_number]
				changing_weapon = true
				mouse_scroll_value = weapon_change_number
			
func fire_weapon_input():
	if Input.is_action_pressed("fire"):
		if reloading_weapon == false:
			if changing_weapon == false:
				var current_weapon = weapons[current_weapon_name]
				if current_weapon != null:
					if current_weapon.ammo_in_weapon > 0:
						if animation_manager.current_state == current_weapon.IDLE_ANIM_NAME:
							animation_manager.set_animation(current_weapon.FIRE_ANIM_NAME)
					else:
						reloading_weapon = true
					
func fire_bullet():
	if changing_weapon == false:
		weapons[current_weapon_name].fire_weapon()
		
func reloading_weapon_input():
	if reloading_weapon == false and changing_weapon == false:
		if Input.is_action_just_pressed("reload"):
			var current_weapon = weapons[current_weapon_name]
			if current_weapon != null:
				if current_weapon.CAN_RELOAD:
					var current_anim_state = animation_manager.current_state
					var is_realoding = false
					for weapon in weapons:
						var weapon_node = weapons[weapon]
						if weapon_node != null:
							if current_anim_state == weapon_node.RELOADING_ANIM_NAME:
								is_realoding = true
					if not is_realoding:
						reloading_weapon = true
						
func joypad_move_input():
	if Input.get_connected_joypads().size() > 0:
		var joypad_vec = Vector2(0, 0)
		joypad_vec = Vector2(Input.get_joy_axis(0, 0), -Input.get_joy_axis(0, 1))
		if joypad_vec.length() < JOYPAD_DEADZONE:
			joypad_vec = Vector2(0, 0)
		else:
			joypad_vec = joypad_vec.normalized() * ((joypad_vec.length() - JOYPAD_DEADZONE) / (1 - JOYPAD_DEADZONE))
		input_movement_vector += joypad_vec

func joypad_view_input(delta):
	if Input.get_mouse_mode() != Input.MOUSE_MODE_CAPTURED:
		return
	var joypad_vec = Vector2()
	if Input.get_connected_joypads().size() > 0:
		joypad_vec = Vector2(Input.get_joy_axis(0, 2), Input.get_joy_axis(0, 3))
		if joypad_vec.length() < JOYPAD_DEADZONE:
			joypad_vec = Vector2(0, 0)
		else:
			joypad_vec = joypad_vec.normalized() * ((joypad_vec.length() - JOYPAD_DEADZONE) / (1 - JOYPAD_DEADZONE))
		rotation_helper.rotate_x(deg2rad(joypad_vec.y * JOYPAD_SENSITIVITY))
		rotate_y(deg2rad(joypad_vec.x * JOYPAD_SENSITIVITY * -1))
		var camera_rot = rotation_helper.rotation_degrees
		camera_rot.x = clamp(camera_rot.x, -70, 70)
		rotation_helper.rotation_degrees = camera_rot

func create_sound(sound_name, postion=null):
	var audio_clone = simple_audio_player.instance()
	var scene_root = get_tree().root.get_children()[0]
	scene_root.add_child(audio_clone)
	audio_clone.play_sound(sound_name, postion)

func bullet_hit(damage, bullet_hit_pos):
	health -= damage